# „TimeManager ” - aplikacja do monitorowania czasu pracy.

### W skrócie :

Program przeznaczony dla pracowników dużej firmy tj. np. Mercor (w której
zatrudnieni są pracownicy do wyrobu elementów przeciwpożarowych – w jednej hali pracuje
około 2000 pracowników na 3 zmiany, na różnych stanowiskach i maszynach
przyporządkowanych do stanowiska). Jego zadaniem jest monitorowanie i zliczanie godzin
pracy, danie możliwości szybkiego przekazania informacji dla przełożonego w postaci
prośby o urlop oraz podania informacji o ewentualnie zaistniałej awarii urządzenia.
Aplikacja jest napisana w języku Java z zewnętrzną bazą danych.

### Przykładowe zrzuty ekranu :
![login.jpeg](https://bitbucket.org/repo/L8kdjo/images/2011603533-login.jpeg)

![admin.jpeg](https://bitbucket.org/repo/L8kdjo/images/660159051-admin.jpeg)

![user.jpeg](https://bitbucket.org/repo/L8kdjo/images/2888105676-user.jpeg)

![leaves.jpeg](https://bitbucket.org/repo/L8kdjo/images/4116774646-leaves.jpeg)