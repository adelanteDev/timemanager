package admin;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import model.Model;
import model.Objects;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Admin extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	public Admin() 
	{
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 384, 265);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		
		JButton btnUsers = new JButton("USERS");
		btnUsers.setBounds(12, 12, 161, 25);
		btnUsers.setBackground(Color.WHITE);
		btnUsers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.userManager.setVisible(true);
			}
		});
		contentPane.add(btnUsers);
		
		JButton btnMachines = new JButton("MACHINES");
		btnMachines.setBackground(Color.WHITE);
		btnMachines.setBounds(12, 49, 161, 25);
		btnMachines.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.machineManager.setVisible(true);
			}
		});
		contentPane.add(btnMachines);
		
		JButton btnPositions = new JButton("POSITIONS");
		btnPositions.setBackground(Color.WHITE);
		btnPositions.setBounds(12, 86, 161, 25);
		btnPositions.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.positionManager.setVisible(true);
			}
		});
		contentPane.add(btnPositions);
		
		JButton btnShowUsers = new JButton("SHOW USERS");
		btnShowUsers.setBackground(Color.WHITE);
		btnShowUsers.setBounds(185, 12, 179, 25);
		btnShowUsers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.usersList.setVisible(true);
			}
		});
		contentPane.add(btnShowUsers);
		
		JButton btnShowMachines = new JButton("SHOW MACHINES");
		btnShowMachines.setBackground(Color.WHITE);
		btnShowMachines.setBounds(185, 86, 179, 25);
		btnShowMachines.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.machinesList.setVisible(true);
			}
		});
		contentPane.add(btnShowMachines);
		
		JButton btnShowAdmins = new JButton("SHOW ADMINS");
		btnShowAdmins.setBackground(Color.WHITE);
		btnShowAdmins.setBounds(185, 49, 179, 25);
		btnShowAdmins.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.adminsList.setVisible(true);
			}
		});
		contentPane.add(btnShowAdmins);
		
		JButton btnShowLeaves = new JButton("SHOW LEAVES");
		btnShowLeaves.setBackground(Color.WHITE);
		btnShowLeaves.setBounds(12, 123, 161, 25);
		btnShowLeaves.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.leavesList.setVisible(true);
			}
		});
		contentPane.add(btnShowLeaves);
		
		JButton btnShowWorkTime = new JButton("SHOW WORK TIME");
		btnShowWorkTime.setBackground(Color.WHITE);
		btnShowWorkTime.setBounds(185, 123, 179, 25);
		btnShowWorkTime.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.userHours.setVisible(true);
			}
		});
		contentPane.add(btnShowWorkTime);
		
		JButton btnLogout = new JButton("LOGOUT");
		btnLogout.setBounds(185, 197, 179, 25);
		btnLogout.setBackground(Color.WHITE);
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.admin.setVisible(false);
				Objects.window.frame.setVisible(true);
				Objects.ResetObjects();
			}
		});
		contentPane.add(btnLogout);
		
		JButton btnShowPositions = new JButton("SHOW POSITIONS");
		btnShowPositions.setBackground(Color.WHITE);
		btnShowPositions.setBounds(185, 160, 179, 25);
		btnShowPositions.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.positionsList.setVisible(true);
			}
		});
		contentPane.add(btnShowPositions);
		
		JButton btnShowComment = new JButton("SHOW COMMENTS");
		btnShowComment.setBounds(12, 160, 161, 25);
		btnShowComment.setBackground(Color.WHITE);
		btnShowComment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.commentsList.setVisible(true);
			}
		});
		contentPane.add(btnShowComment);
		
		Model.ImagePanel panel = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
		getContentPane().add(panel);
	}
}
