package admin;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.ListSelectionModel;

import model.Model;
import model.Objects;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CommentsList extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable tTable;

	public CommentsList() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 388, 290);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		JButton btnShow = new JButton("SHOW");
		btnShow.setBounds(242, 9, 124, 20);
		btnShow.setBackground(Color.WHITE);
		btnShow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshTable();
			}
		});
		contentPane.add(btnShow);

		JScrollPane spTable;
		tTable = new JTable();
		JPanel pTable = new JPanel();
		pTable.setBounds(12, 39, 354, 173);
		pTable.setLayout(new BorderLayout());
		tTable.setModel(new DefaultTableModel(new Object[9][10],
				new String[] { "NICK", "NAME", "SURNAME", "MACHINE", "DATE",
						"TIME", "COMMENT" }));
		spTable = new JScrollPane();
		spTable.setViewportView(tTable);
		pTable.add(spTable, BorderLayout.CENTER);
		tTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		getContentPane().add(pTable);

		JButton btnE = new JButton("EXPORT TO FILE");
		btnE.setBackground(Color.WHITE);
		btnE.setBounds(12, 224, 171, 25);
		btnE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					createFile();
				} catch (FileNotFoundException | UnsupportedEncodingException e1) {
					e1.printStackTrace();
				}
			}
		});
		contentPane.add(btnE);

		JButton btnBack = new JButton("BACK");
		btnBack.setBackground(Color.WHITE);
		btnBack.setBounds(195, 224, 171, 25);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.commentsList.setVisible(false);
			}
		});
		contentPane.add(btnBack);

		Model.ImagePanel panel = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
		getContentPane().add(panel);
	}

	public void refreshTable() {
		DefaultTableModel tableModel = (DefaultTableModel) tTable.getModel();
		tableModel.setNumRows(0);
		Objects.connection = Model.Connection();
		PreparedStatement st1 = null;
		PreparedStatement st2 = null;
		PreparedStatement st3 = null;
		ResultSet result1 = null;
		ResultSet result2 = null;
		try {
			st1 = Objects.connection
					.prepareStatement("SELECT id_user, comment, date, time, id_machine FROM comments;");
			ResultSet result = st1.executeQuery();
			int i = 0;
			while (result.next()) {
				tableModel.addRow(new Object[] { 1 });
				tTable.setValueAt(result.getString(2), i, 6);
				tTable.setValueAt(result.getDate(3), i, 4);
				tTable.setValueAt(result.getTime(4), i, 5);
				st2 = Objects.connection
						.prepareStatement("SELECT nick, name, surname FROM users WHERE id_user="
								+ result.getInt(1) + ";");
				result1 = st2.executeQuery();
				while (result1.next()) {
					tTable.setValueAt(result1.getString(1), i, 0);
					tTable.setValueAt(result1.getString(2), i, 1);
					tTable.setValueAt(result1.getString(3), i, 2);
				}
				st3 = Objects.connection
						.prepareStatement("SELECT name FROM machines WHERE id_machine="
								+ result.getInt(5) + ";");
				result2 = st3.executeQuery();
				while (result2.next()) {
					tTable.setValueAt(result2.getString(1), i, 3);
				}
				i++;
			}
			st1.close();
			st2.close();
			st3.close();
			result.close();
			result1.close();
			result2.close();
			Objects.connection.close();
		} catch (SQLException e1) {
			// e1.printStackTrace();
		}
	}

	public void createFile() throws FileNotFoundException,
			UnsupportedEncodingException {

		Objects.connection = Model.Connection();
		PreparedStatement st1 = null;
		PreparedStatement st2 = null;
		PreparedStatement st3 = null;
		try {
			st1 = Objects.connection
					.prepareStatement("SELECT id_user, comment, date, time, id_machine FROM comments;");
			ResultSet result = st1.executeQuery();
			ResultSet result1 = null;
			ResultSet result2 = null;
			PrintWriter writer = new PrintWriter("Comments List.txt", "UTF-8");
			writer.println("Actual comments list:\n");
			writer.println("Nick.\tName\tSurname\t\tMachine\tDate\tTime\tComment\n");
			while (result.next()) {
				st2 = Objects.connection
						.prepareStatement("SELECT nick, name, surname FROM users WHERE id_user="
								+ result.getInt(1) + ";");
				result1 = st2.executeQuery();
				while (result1.next()) {
					st3 = Objects.connection
							.prepareStatement("SELECT name FROM machines WHERE id_machine="
									+ result.getInt(5) + ";");
					result2 = st3.executeQuery();
					while (result2.next()) {
						writer.println(result1.getString(1) + "\t"
								+ result1.getString(2) + "\t"
								+ result1.getString(3) + "\t"
								+ result2.getString(1) + "\t"
								+ result.getDate(3) + "\t" + result.getTime(4)
								+ "\t" + result.getString(2));
					}
				}
			}
			writer.close();
			result.close();
			result1.close();
			result2.close();
			st1.close();
			st2.close();
			st3.close();
			Objects.connection.close();
		} catch (SQLException e1) {
			// e1.printStackTrace();
		}
	}
}