package admin;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SpinnerListModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JSpinner;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import model.Model;
import model.Objects;

public class MachineManager extends JFrame 
{
	private static final long serialVersionUID = 1L;
	public static JTextField textID;
	public static JTextField textName;
	public static JTextField textNick;
	public static JSpinner spinner;

	public MachineManager() {
		initialize();
	}

	private void initialize() 
	{
		setBounds(100, 100, 244, 260);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		setResizable(false);
		setLocationRelativeTo(null);

		JLabel lblId = new JLabel("ID :");
		lblId.setForeground(Color.WHITE);
		lblId.setBounds(12, 12, 70, 15);
		getContentPane().add(lblId);

		JLabel lblName = new JLabel("Name :");
		lblName.setForeground(Color.WHITE);
		lblName.setBounds(12, 39, 70, 15);
		getContentPane().add(lblName);

		JLabel lblStatus = new JLabel("Status :");
		lblStatus.setForeground(Color.WHITE);
		lblStatus.setBounds(12, 66, 70, 15);
		getContentPane().add(lblStatus);

		JLabel lblPassword = new JLabel("ID Position :");
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setBounds(12, 93, 97, 15);
		getContentPane().add(lblPassword);

		textID = new JTextField();
		textID.setBounds(111, 10, 114, 19);
		getContentPane().add(textID);
		textID.setColumns(10);
		textID.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				if (!textID.getText().isEmpty()) {
					Objects.connection = Model.Connection();
					PreparedStatement st1 = null;
					try {
						st1 = Objects.connection
								.prepareStatement("SELECT name, status, id_position FROM machines WHERE id_machine=?;");
						st1.setInt(1, Integer.parseInt(textID.getText()));
						ResultSet result = st1.executeQuery();
						int position_id = 0;
						while (result.next()) {
							textName.setText(result.getString(1));
							textNick.setText(result.getString(2));
							position_id = result.getInt(3);
						}
						st1 = Objects.connection
								.prepareStatement("SELECT name FROM positions WHERE id_position="
										+ position_id + ";");
						result = st1.executeQuery();
						while (result.next()) {
							spinner.setValue(result.getString(1));
						}
						result.close();
						Objects.connection.close();
						Objects.findMachine.setVisible(false);
					} catch (SQLException e1) {
						// e1.printStackTrace();
					}
				}

			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				if (!textID.getText().isEmpty()) {
					Objects.connection = Model.Connection();
					PreparedStatement st1 = null;
					try {
						st1 = Objects.connection
								.prepareStatement("SELECT name, status, id_position FROM machines WHERE id_machine=?;");
						st1.setInt(1, Integer.parseInt(textID.getText()));
						ResultSet result = st1.executeQuery();
						int position_id = 0;
						while (result.next()) {
							textName.setText(result.getString(1));
							textNick.setText(result.getString(2));
							position_id = result.getInt(3);
						}
						st1 = Objects.connection
								.prepareStatement("SELECT name FROM positions WHERE id_position="
										+ position_id + ";");
						result = st1.executeQuery();
						while (result.next()) {
							spinner.setValue(result.getString(1));
						}
						result.close();
						Objects.connection.close();
						Objects.findMachine.setVisible(false);
					} catch (SQLException e1) {
						// e1.printStackTrace();
					}
				}
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				textName.setText("");
				textNick.setText("");
				spinner.setValue("");
			}
		});

		textName = new JTextField();
		textName.setBounds(111, 37, 114, 19);
		getContentPane().add(textName);
		textName.setColumns(10);

		textNick = new JTextField("OK");
		textNick.setBounds(111, 64, 114, 19);
		getContentPane().add(textNick);
		textNick.setColumns(10);

		JButton btnNew = new JButton("NEW");
		btnNew.setBackground(Color.WHITE);
		btnNew.setBounds(12, 120, 104, 25);
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textName.getText().isEmpty()
						&& !textNick.getText().isEmpty()) {
					Objects.connection = Model.Connection();
					PreparedStatement st1 = null;
					try {
						st1 = Objects.connection
								.prepareStatement("SELECT name FROM machines;");
						ResultSet result = st1.executeQuery();
						boolean status = true;
						while (result.next()) {
							if (result.getString(1).equals(textName.getText()))
								status = false;
						}
						if (status == true) {
							st1 = Objects.connection
									.prepareStatement("INSERT INTO machines (name, status, id_position) VALUES (?,?,"
											+ Model.getPositionId(spinner
													.getValue().toString())
											+ ");");
							st1.setString(1, textName.getText());
							st1.setString(2, textNick.getText());
							st1.execute();
							Objects.errorComment.lblCommentPass
									.setText("INSERTED");
							Objects.errorComment.setVisible(true);
						} else {
							Objects.errorComment.lblCommentPass
									.setText("NOT INSERTED");
							Objects.errorComment.setVisible(true);
						}
						result.close();
						Objects.connection.close();
						Objects.errorComment.lblCommentPass.setText("INSERTED");
						Objects.errorComment.setVisible(true);
					} catch (SQLException e1) {
						// e1.printStackTrace();
						Objects.errorComment.lblCommentPass
								.setText("NOT INSERTED");
						Objects.errorComment.setVisible(true);
					}
				} else {
					Objects.errorComment.lblCommentPass.setText("CHECK FIELDS");

					Objects.errorComment.setVisible(true);
				}
			}
		});
		getContentPane().add(btnNew);

		JButton btnUpdate = new JButton("UPDATE");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textName.getText().isEmpty()
						&& !textNick.getText().isEmpty()
						&& !textID.getText().isEmpty()) {
					Objects.connection = Model.Connection();
					PreparedStatement st1 = null;
					try {
						st1 = Objects.connection
								.prepareStatement("SELECT name FROM machines WHERE id_machine!=?;");
						st1.setInt(1, Integer.parseInt(textID.getText()));
						ResultSet result = st1.executeQuery();
						boolean status = true;
						while (result.next()) {
							if (result.getString(1).equals(textName.getText()))
								status = false;
						}
						if (status == true) {
							st1 = Objects.connection
									.prepareStatement("UPDATE machines SET name=?, status=?, id_position="
											+ Model.getPositionId(spinner
													.getValue().toString())
											+ " WHERE id_machine=?;");
							st1.setString(1, textName.getText());
							st1.setString(2, textNick.getText());
							st1.setInt(3, Integer.parseInt(textID.getText()));
							st1.execute();
							Objects.errorComment.lblCommentPass
									.setText("UPDATED");
							Objects.errorComment.setVisible(true);
						} else {
							Objects.errorComment.lblCommentPass
									.setText("NOT UPDATED");
							Objects.errorComment.setVisible(true);
						}
						result.close();
						Objects.connection.close();
					} catch (SQLException e1) {

						// e1.printStackTrace();
					}
				}

				else {
					Objects.errorComment.lblCommentPass.setText("CHECK FIELDS");

					Objects.errorComment.setVisible(true);
				}
			}
		});
		btnUpdate.setBackground(Color.WHITE);
		btnUpdate.setBounds(121, 120, 104, 25);
		getContentPane().add(btnUpdate);

		JButton buttonDelete = new JButton("DELETE");
		buttonDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textID.getText().isEmpty()) {
					Objects.connection = Model.Connection();
					PreparedStatement st1 = null;
					try {
						st1 = Objects.connection
								.prepareStatement("DELETE FROM machines WHERE id_machine=?;");
						st1.setInt(1, Integer.parseInt(textID.getText()));
						st1.execute();
						Objects.connection.close();
						Objects.errorComment.lblCommentPass.setText("DELETED");
						Objects.errorComment.setVisible(true);
					} catch (SQLException e1) {
						Objects.errorComment.lblCommentPass
								.setText("NOT DELETED");
						Objects.errorComment.setVisible(true);
						// e1.printStackTrace();
					}
				}

				else {
					Objects.errorComment.lblCommentPass
							.setText("CHECK FIELD ID");

					Objects.errorComment.setVisible(true);
				}
			}
		});
		buttonDelete.setBackground(Color.WHITE);
		buttonDelete.setBounds(12, 157, 104, 25);
		getContentPane().add(buttonDelete);

		JButton btnFind = new JButton("FIND");
		btnFind.setBackground(Color.WHITE);
		btnFind.setBounds(121, 157, 104, 25);
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.findMachine.setVisible(true);
			}
		});
		getContentPane().add(btnFind);

		JButton btnBack = new JButton("BACK");
		btnBack.setBackground(Color.WHITE);
		btnBack.setBounds(12, 194, 213, 25);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.machineManager.setVisible(false);
			}
		});
		getContentPane().add(btnBack);

		spinner = new JSpinner();
		spinner.setModel(new SpinnerListModel(Model.getPositions()));
		spinner.setBounds(111, 91, 114, 20);
		getContentPane().add(spinner);

		Model.ImagePanel panel = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
		getContentPane().add(panel);

	}
}