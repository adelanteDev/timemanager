package admin;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import model.Model;
import model.Objects;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PositionManager extends JFrame 
{
	private static final long serialVersionUID = 1L;
	public static JTextField textID;
	public static JTextField textName;

	public PositionManager() {
		initialize();
	}

	private void initialize() 
	{
		setBounds(100, 100, 245, 209);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		setResizable(false);
		setLocationRelativeTo(null);

		JLabel lblId = new JLabel("ID :");
		lblId.setForeground(Color.WHITE);
		lblId.setBounds(12, 12, 70, 15);
		getContentPane().add(lblId);

		JLabel lblName = new JLabel("Name :");
		lblName.setForeground(Color.WHITE);
		lblName.setBounds(12, 39, 70, 15);
		getContentPane().add(lblName);

		textID = new JTextField();
		textID.setBounds(111, 10, 114, 19);
		getContentPane().add(textID);
		textID.setColumns(10);

		textName = new JTextField();
		textName.setBounds(111, 37, 114, 19);
		getContentPane().add(textName);
		textName.setColumns(10);

		textID.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				if (!textID.getText().isEmpty()) {
					Objects.connection = Model.Connection();
					PreparedStatement st1 = null;
					try {
						st1 = Objects.connection
								.prepareStatement("SELECT name FROM positions WHERE id_position=?;");
						st1.setInt(1, Integer.parseInt(textID.getText()));
						ResultSet result = st1.executeQuery();
						while (result.next()) {
							textName.setText(result.getString(1));
						}
						result.close();
						Objects.connection.close();
						Objects.findPosition.setVisible(false);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}

			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				if (!textID.getText().isEmpty()) {
					Objects.connection = Model.Connection();
					PreparedStatement st1 = null;
					try {
						st1 = Objects.connection
								.prepareStatement("SELECT name FROM positions WHERE id_position=?;");
						st1.setInt(1, Integer.parseInt(textID.getText()));
						ResultSet result = st1.executeQuery();
						while (result.next()) {
							textName.setText(result.getString(1));
						}
						result.close();
						Objects.connection.close();
						Objects.findPosition.setVisible(false);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				textName.setText("");
			}
		});

		JButton btnNew = new JButton("NEW");
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textName.getText().isEmpty()) {
					Objects.connection = Model.Connection();
					PreparedStatement st1 = null;
					try {
						st1 = Objects.connection
								.prepareStatement("SELECT name FROM positions;");
						ResultSet result = st1.executeQuery();
						boolean status = true;
						while (result.next()) {
							if (result.getString(1).equals(textName.getText()))
								status = false;
						}
						if (status == true) {
							st1 = Objects.connection
									.prepareStatement("INSERT INTO positions (name) VALUES (?);");
							st1.setString(1, textName.getText());
							st1.execute();
							Objects.errorComment.lblCommentPass
									.setText("INSERTED");
							Objects.errorComment.setVisible(true);
						} else {
							Objects.errorComment.lblCommentPass
									.setText("NOT INSERTED");
							Objects.errorComment.setVisible(true);
						}
						result.close();
						Objects.connection.close();
					} catch (SQLException e1) {
						// e1.printStackTrace();
					}
				} else {
					Objects.errorComment.lblCommentPass.setText("CHECK FIELDS");

					Objects.errorComment.setVisible(true);
				}
			}
		});
		btnNew.setBackground(Color.WHITE);
		btnNew.setBounds(12, 68, 104, 25);
		getContentPane().add(btnNew);

		JButton btnUpdate = new JButton("UPDATE");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textName.getText().isEmpty()
						&& !textID.getText().isEmpty()) {
					Objects.connection = Model.Connection();
					PreparedStatement st1 = null;
					try {
						st1 = Objects.connection
								.prepareStatement("SELECT name FROM positions WHERE id_position!=?;");
						st1.setInt(1, Integer.parseInt(textID.getText()));
						ResultSet result = st1.executeQuery();
						boolean status = true;
						while (result.next()) {
							if (result.getString(1).equals(textName.getText()))
								status = false;
						}
						if (status == true) {
							st1 = Objects.connection
									.prepareStatement("UPDATE positions SET name=? WHERE id_position=?;");
							st1.setString(1, textName.getText());
							st1.setInt(2, Integer.parseInt(textID.getText()));
							st1.execute();
							Objects.errorComment.lblCommentPass
									.setText("UPDATED");
							Objects.errorComment.setVisible(true);
						} else {
							Objects.errorComment.lblCommentPass
									.setText("NOT UPDATED");
							Objects.errorComment.setVisible(true);
						}
						result.close();
						Objects.connection.close();
					} catch (SQLException e1) {
						// e1.printStackTrace();
					}
				} else {
					Objects.errorComment.lblCommentPass.setText("CHECK FIELDS");

					Objects.errorComment.setVisible(true);
				}
			}
		});
		btnUpdate.setBackground(Color.WHITE);
		btnUpdate.setBounds(121, 68, 104, 25);
		getContentPane().add(btnUpdate);

		JButton buttonDelete = new JButton("DELETE");
		buttonDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textID.getText().isEmpty()) {
					Objects.connection = Model.Connection();
					PreparedStatement st1 = null;
					try {
						st1 = Objects.connection
								.prepareStatement("DELETE FROM positions WHERE id_position=?;");
						st1.setInt(1, Integer.parseInt(textID.getText()));
						st1.execute();
						Objects.connection.close();
						Objects.errorComment.lblCommentPass.setText("DELETED");
						Objects.errorComment.setVisible(true);
					} catch (SQLException e1) {
						Objects.errorComment.lblCommentPass
								.setText("NOT DELETED");
						Objects.errorComment.setVisible(true);
						// e1.printStackTrace();
					}
				} else {
					Objects.errorComment.lblCommentPass.setText("CHECK FIELDS");

					Objects.errorComment.setVisible(true);
				}
			}
		});
		buttonDelete.setBackground(Color.WHITE);
		buttonDelete.setBounds(12, 102, 104, 25);
		getContentPane().add(buttonDelete);

		JButton btnFind = new JButton("FIND");
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.findPosition.setVisible(true);
			}
		});
		btnFind.setBackground(Color.WHITE);
		btnFind.setBounds(121, 102, 104, 25);
		getContentPane().add(btnFind);

		JButton btnBack = new JButton("BACK");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.positionManager.setVisible(false);
			}
		});
		btnBack.setBackground(Color.WHITE);
		btnBack.setBounds(12, 139, 213, 25);
		getContentPane().add(btnBack);

		Model.ImagePanel panel = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
		getContentPane().add(panel);

	}
}