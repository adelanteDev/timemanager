package admin;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.ListSelectionModel;

import model.Model;
import model.Objects;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserHours extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTable tTable;

	public UserHours() 
	{
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 388, 290);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNickname = new JLabel("Nickname :");
		lblNickname.setForeground(Color.WHITE);
		lblNickname.setBounds(12, 12, 86, 15);
		contentPane.add(lblNickname);

		textField = new JTextField();
		textField.setBounds(101, 10, 129, 19);
		contentPane.add(textField);
		textField.setColumns(10);

		JButton btnShow = new JButton("SHOW");
		btnShow.setBackground(Color.WHITE);
		btnShow.setBounds(242, 9, 124, 20);
		btnShow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textField.getText().isEmpty()) {
					refreshTable(textField.getText());
				}
			}
		});
		contentPane.add(btnShow);

		JScrollPane spTable;
		tTable = new JTable();
		JPanel pTable = new JPanel();
		pTable.setBounds(12, 39, 354, 173);
		pTable.setLayout(new BorderLayout());
		tTable.setModel(new DefaultTableModel(new Object[9][10], new String[] {
				"ID", "NICK", "NAME", "SURNAME", "DAY", "WORK TIME" }));
		spTable = new JScrollPane();
		spTable.setViewportView(tTable);
		pTable.add(spTable, BorderLayout.CENTER);
		tTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		getContentPane().add(pTable);

		JButton btnE = new JButton("EXPORT TO FILE");
		btnE.setBackground(Color.WHITE);
		btnE.setBounds(12, 224, 171, 25);
		btnE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textField.getText().isEmpty()) {
					try {
						createFile(textField.getText());
					} catch (FileNotFoundException
							| UnsupportedEncodingException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		contentPane.add(btnE);

		JButton btnBack = new JButton("BACK");
		btnBack.setBounds(195, 224, 171, 25);
		btnBack.setBackground(Color.WHITE);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.userHours.setVisible(false);
			}
		});
		contentPane.add(btnBack);

		Model.ImagePanel panel = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
		getContentPane().add(panel);
	}

	public void refreshTable(String nick) {
		DefaultTableModel tableModel = (DefaultTableModel) tTable.getModel();
		tableModel.setNumRows(0);

		int id_user = 0;
		String name = "";
		String surname = "";

		Objects.connection = Model.Connection();
		PreparedStatement st1 = null;
		try {
			st1 = Objects.connection
					.prepareStatement("SELECT id_user, name, surname FROM users WHERE nick=?;");
			st1.setString(1, nick);
			ResultSet result = st1.executeQuery();
			while (result.next()) {
				id_user = result.getInt(1);
				name = result.getString(2);
				surname = result.getString(3);
			}
			st1 = Objects.connection
					.prepareStatement("SELECT date, seconds FROM hours WHERE id_user=?;");
			st1.setInt(1, id_user);
			result = st1.executeQuery();
			int i = 0;
			while (result.next()) {
				tableModel.addRow(new Object[] { 1 });
				tTable.setValueAt(id_user, i, 0);
				tTable.setValueAt(nick, i, 1);
				tTable.setValueAt(name, i, 2);
				tTable.setValueAt(surname, i, 3);
				tTable.setValueAt(result.getDate(1), i, 4);
				tTable.setValueAt((float) result.getInt(2) / 3600, i, 5);
				i++;
			}
			result.close();
			Objects.connection.close();
		} catch (SQLException e1) {
			// e1.printStackTrace();
		}
	}

	public void createFile(String nick) throws FileNotFoundException,
			UnsupportedEncodingException {
		int id_user = 0;
		String name = "";
		String surname = "";

		Objects.connection = Model.Connection();
		PreparedStatement st1 = null;
		try {
			st1 = Objects.connection
					.prepareStatement("SELECT id_user, name, surname FROM users WHERE nick=?;");
			st1.setString(1, nick);
			ResultSet result = st1.executeQuery();
			while (result.next()) {
				id_user = result.getInt(1);
				name = result.getString(2);
				surname = result.getString(3);
			}
			st1 = Objects.connection
					.prepareStatement("SELECT date, seconds FROM hours WHERE id_user=?;");
			st1.setInt(1, id_user);
			result = st1.executeQuery();
			int i = 1;
			float sum = 0;
			PrintWriter writer = new PrintWriter(name + " " + surname
					+ " Worked Hours.txt", "UTF-8");
			writer.println(name + " " + surname + " - nick: " + nick);
			writer.println("Lp.\tDate\t\tWorked Hours\n");
			while (result.next()) {
				sum = sum + ((float) result.getInt(2) / 3600);
				writer.println(i + ".\t" + result.getDate(1) + "\t"
						+ (float) result.getInt(2) / 3600);
				i++;
			}
			writer.println("Hours sum: " + sum + " h");
			writer.close();
			result.close();
			Objects.connection.close();
		} catch (SQLException e1) {
			// e1.printStackTrace();
		}
	}

}