package admin;

import java.awt.Color;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.SpinnerListModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import model.Model;
import model.Objects;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserManager extends JFrame 
{
	private static final long serialVersionUID = 1L;
	public static JTextField textID;
	public static JTextField textName;
	public static JTextField textSurname;
	public static JTextField textNick;
	public static JTextField textPassword;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	public static JSpinner spinner;
	public static JTextField textStatus;
	public static JRadioButton rdbtnAdmin;
	public static JRadioButton radioUser;
	public static JLabel lblPosition;

	public UserManager() {
		initialize();
	}

	public void transparentButton(JRadioButton button) 
	{
		button.setOpaque(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
	}

	private void initialize() 
	{
		setBounds(100, 100, 244, 319);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		setResizable(false);
		setLocationRelativeTo(null);

		JLabel lblId = new JLabel("ID :");
		lblId.setForeground(Color.WHITE);
		lblId.setBounds(12, 33, 70, 15);
		getContentPane().add(lblId);

		JLabel lblName = new JLabel("Name :");
		lblName.setForeground(Color.WHITE);
		lblName.setBounds(12, 60, 70, 15);
		getContentPane().add(lblName);

		JLabel lblNewLabel = new JLabel("Surname :");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(12, 87, 87, 15);
		getContentPane().add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Nick :");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setBounds(12, 114, 70, 15);
		getContentPane().add(lblNewLabel_1);

		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setBounds(12, 141, 97, 15);
		getContentPane().add(lblPassword);

		textID = new JTextField();
		textID.setBounds(111, 31, 114, 19);
		getContentPane().add(textID);
		textID.setColumns(10);

		textID.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				if (radioUser.isSelected()) {
					if (!textID.getText().isEmpty()) {
						Objects.connection = Model.Connection();
						PreparedStatement st1 = null;
						try {
							st1 = Objects.connection
									.prepareStatement("SELECT id_user, name, surname, nick, passwd, status, id_position FROM users WHERE id_user=?;");
							st1.setInt(1, Integer.parseInt(textID.getText()));
							ResultSet result = st1.executeQuery();
							int position_id = 0;
							while (result.next()) {
								textName.setText(result.getString(2));
								textSurname.setText(result.getString(3));
								textNick.setText(result.getString(4));
								textPassword.setText(result.getString(5));
								textStatus.setText(result.getString(6));
								position_id = result.getInt(7);
							}
							st1 = Objects.connection
									.prepareStatement("SELECT name FROM positions WHERE id_position="
											+ position_id + ";");
							result = st1.executeQuery();
							while (result.next()) {
								spinner.setValue(result.getString(1));
							}
							radioUser.setSelected(true);
							spinner.setVisible(true);
							lblPosition.setVisible(true);

							result.close();
							Objects.connection.close();
						} catch (SQLException e1) {
							// e1.printStackTrace();
						}
					} else {
						Objects.errorComment.lblCommentPass
								.setText("CHECK FIELD ID");
						Objects.errorComment.setVisible(true);
					}
				}
				if (rdbtnAdmin.isSelected()) {
					if (!textID.getText().isEmpty()) {
						Objects.connection = Model.Connection();
						PreparedStatement st1 = null;
						try {
							st1 = Objects.connection
									.prepareStatement("SELECT id_admin, name, surname, nick, passwd, status FROM admins WHERE id_admin=?;");
							st1.setInt(1, Integer.parseInt(textID.getText()));
							ResultSet result = st1.executeQuery();
							while (result.next()) {
								textName.setText(result.getString(2));
								textSurname.setText(result.getString(3));
								textNick.setText(result.getString(4));
								textPassword.setText(result.getString(5));
								textStatus.setText(result.getString(6));
							}
							rdbtnAdmin.setSelected(true);
							spinner.setVisible(false);
							lblPosition.setVisible(false);
							result.close();
							Objects.connection.close();
						} catch (SQLException e1) {
							// e1.printStackTrace();
						}
					} else {
						Objects.errorComment.lblCommentPass
								.setText("CHECK FIELD ID");
						Objects.errorComment.setVisible(true);
					}
				}

			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				if (radioUser.isSelected()) {
					if (!textID.getText().isEmpty()) {
						Objects.connection = Model.Connection();
						PreparedStatement st1 = null;
						try {
							st1 = Objects.connection
									.prepareStatement("SELECT id_user, name, surname, nick, passwd, status, id_position FROM users WHERE id_user=?;");
							st1.setInt(1, Integer.parseInt(textID.getText()));
							ResultSet result = st1.executeQuery();
							int position_id = 0;
							while (result.next()) {
								textName.setText(result.getString(2));
								textSurname.setText(result.getString(3));
								textNick.setText(result.getString(4));
								textPassword.setText(result.getString(5));
								textStatus.setText(result.getString(6));
								position_id = result.getInt(7);
							}
							st1 = Objects.connection
									.prepareStatement("SELECT name FROM positions WHERE id_position="
											+ position_id + ";");
							result = st1.executeQuery();
							while (result.next()) {
								spinner.setValue(result.getString(1));
							}
							radioUser.setSelected(true);
							spinner.setVisible(true);
							lblPosition.setVisible(true);

							result.close();
							Objects.connection.close();
						} catch (SQLException e1) {
							// e1.printStackTrace();
						}
					} else
						Objects.errorComment.setVisible(true);
				}
				if (rdbtnAdmin.isSelected()) {
					if (!textID.getText().isEmpty()) {
						Objects.connection = Model.Connection();
						PreparedStatement st1 = null;
						try {
							st1 = Objects.connection
									.prepareStatement("SELECT id_admin, name, surname, nick, passwd, status FROM admins WHERE id_admin=?;");
							st1.setInt(1, Integer.parseInt(textID.getText()));
							ResultSet result = st1.executeQuery();
							while (result.next()) {
								textName.setText(result.getString(2));
								textSurname.setText(result.getString(3));
								textNick.setText(result.getString(4));
								textPassword.setText(result.getString(5));
								textStatus.setText(result.getString(6));
							}
							rdbtnAdmin.setSelected(true);
							spinner.setVisible(false);
							lblPosition.setVisible(false);
							result.close();
							Objects.connection.close();
						} catch (SQLException e1) {
							// e1.printStackTrace();
						}
					} else {
						Objects.errorComment.lblCommentPass
								.setText("CHECK FIELD ID");
						Objects.errorComment.setVisible(true);
					}
				}
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				textName.setText("");
				textNick.setText("");
				textPassword.setText("");
				textStatus.setText("");
				textSurname.setText("");
				// spinner.setValue("");
			}
		});

		textName = new JTextField();
		textName.setBounds(111, 58, 114, 19);
		getContentPane().add(textName);
		textName.setColumns(10);

		textSurname = new JTextField();
		textSurname.setBounds(111, 85, 114, 19);
		getContentPane().add(textSurname);
		textSurname.setColumns(10);

		textNick = new JTextField();
		textNick.setBounds(111, 112, 114, 19);
		getContentPane().add(textNick);
		textNick.setColumns(10);

		textPassword = new JTextField();
		textPassword.setBounds(111, 138, 114, 19);
		getContentPane().add(textPassword);
		textPassword.setColumns(10);

		JButton btnNew = new JButton("NEW");
		btnNew.setBackground(Color.WHITE);
		btnNew.setBounds(12, 214, 104, 20);
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rdbtnAdmin.isSelected()) {
					if (!textName.getText().isEmpty()
							&& !textNick.getText().isEmpty()
							&& !textPassword.getText().isEmpty()
							&& !textSurname.getText().isEmpty()
							&& !textStatus.getText().isEmpty()) {
						Objects.connection = Model.Connection();
						PreparedStatement st1 = null;
						try {
							st1 = Objects.connection
									.prepareStatement("SELECT nick FROM admins;");
							ResultSet result = st1.executeQuery();
							boolean status = true;
							while (result.next()) {
								if (result.getString(1).equals(
										textNick.getText()))
									status = false;
							}
							if (status == true) {
								st1 = Objects.connection
										.prepareStatement("INSERT INTO admins (name, nick, surname, status, passwd) VALUES (?,?,?,?,?);");
								st1.setString(1, textName.getText());
								st1.setString(2, textNick.getText());
								st1.setString(3, textSurname.getText());
								st1.setString(4, textStatus.getText());
								st1.setString(5, textPassword.getText());
								st1.execute();
								Objects.errorComment.lblCommentPass
										.setText("INSERTED");
								Objects.errorComment.setVisible(true);
							} else {
								Objects.errorComment.lblCommentPass
										.setText("NOT INSERTED");
								Objects.errorComment.setVisible(true);
							}
							result.close();
							Objects.connection.close();
						} catch (SQLException e1) {
							// e1.printStackTrace();
						}
					} else {
						Objects.errorComment.lblCommentPass
								.setText("CHECK FIELDS");

						Objects.errorComment.setVisible(true);
					}
				}
				if (radioUser.isSelected()) {
					if (!textName.getText().isEmpty()
							&& !textNick.getText().isEmpty()
							&& !textPassword.getText().isEmpty()
							&& !textSurname.getText().isEmpty()
							&& !textStatus.getText().isEmpty()) {
						Objects.connection = Model.Connection();
						PreparedStatement st1 = null;
						try {
							st1 = Objects.connection
									.prepareStatement("SELECT nick FROM users;");
							ResultSet result = st1.executeQuery();
							boolean status = true;
							while (result.next()) {
								if (result.getString(1).equals(
										textNick.getText()))
									status = false;
							}
							if (status == true) {
								st1 = Objects.connection
										.prepareStatement("INSERT INTO users (name, nick, surname, status, passwd, id_position) VALUES (?,?,?,?,?,"
												+ Model.getPositionId(spinner
														.getValue().toString())
												+ ");");
								st1.setString(1, textName.getText());
								st1.setString(2, textNick.getText());
								st1.setString(3, textSurname.getText());
								st1.setString(4, textStatus.getText());
								st1.setString(5, textPassword.getText());
								st1.execute();
								Objects.errorComment.lblCommentPass
										.setText("INSERTED");
								Objects.errorComment.setVisible(true);
							} else {
								Objects.errorComment.lblCommentPass
										.setText("NOT INSERTED");
								Objects.errorComment.setVisible(true);
							}
							result.close();
							Objects.connection.close();
						} catch (SQLException e1) {
							// e1.printStackTrace();
						}
					} else {
						Objects.errorComment.lblCommentPass
								.setText("CHECK FIELDS");

						Objects.errorComment.setVisible(true);
					}
				}
			}
		});
		getContentPane().add(btnNew);

		JButton btnUpdate = new JButton("UPDATE");
		btnUpdate.setBackground(Color.WHITE);
		btnUpdate.setBounds(121, 214, 104, 20);
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rdbtnAdmin.isSelected()) {
					if (!textName.getText().isEmpty()
							&& !textNick.getText().isEmpty()
							&& !textID.getText().isEmpty()
							&& !textPassword.getText().isEmpty()
							&& !textSurname.getText().isEmpty()
							&& !textStatus.getText().isEmpty()) {
						Objects.connection = Model.Connection();
						PreparedStatement st1 = null;
						try {
							st1 = Objects.connection
									.prepareStatement("SELECT nick FROM admins WHERE id_admin!=?;");
							st1.setInt(1, Integer.parseInt(textID.getText()));
							ResultSet result = st1.executeQuery();
							boolean status = true;
							while (result.next()) {
								if (result.getString(1).equals(
										textNick.getText()))
									status = false;
							}
							if (status == true) {
								st1 = Objects.connection
										.prepareStatement("UPDATE admins SET name=?, status=?, surname=?, nick=?, passwd=?  WHERE id_admin=?;");
								st1.setString(1, textName.getText());
								st1.setString(2, textStatus.getText());
								st1.setString(3, textSurname.getText());
								st1.setString(4, textNick.getText());
								st1.setString(5, textPassword.getText());
								st1.setInt(6,
										Integer.parseInt(textID.getText()));
								st1.execute();
								Objects.errorComment.lblCommentPass
										.setText("UPDATED");
								Objects.errorComment.setVisible(true);
							} else {
								Objects.errorComment.lblCommentPass
										.setText("NOT UPDATED");
								Objects.errorComment.setVisible(true);
							}
							result.close();
							Objects.connection.close();
						} catch (SQLException e1) {
							// e1.printStackTrace();
						}
					} else {
						Objects.errorComment.lblCommentPass
								.setText("CHECK FIELDS");

						Objects.errorComment.setVisible(true);
					}
				}
				if (radioUser.isSelected()) {
					if (!textName.getText().isEmpty()
							&& !textNick.getText().isEmpty()
							&& !textID.getText().isEmpty()
							&& !textPassword.getText().isEmpty()
							&& !textSurname.getText().isEmpty()
							&& !textStatus.getText().isEmpty()) {
						Objects.connection = Model.Connection();
						PreparedStatement st1 = null;
						try {
							st1 = Objects.connection
									.prepareStatement("SELECT nick FROM users WHERE id_user!=?;");
							st1.setInt(1, Integer.parseInt(textID.getText()));
							ResultSet result = st1.executeQuery();
							boolean status = true;
							while (result.next()) {
								if (result.getString(1).equals(
										textNick.getText()))
									status = false;
							}
							if (status == true) {
								st1 = Objects.connection
										.prepareStatement("UPDATE users SET name=?, status=?, surname=?, nick=?, passwd=?, id_position="
												+ Model.getPositionId(spinner
														.getValue().toString())
												+ "  WHERE id_user=?;");
								st1.setString(1, textName.getText());
								st1.setString(2, textStatus.getText());
								st1.setString(3, textSurname.getText());
								st1.setString(4, textNick.getText());
								st1.setString(5, textPassword.getText());
								st1.setInt(6,
										Integer.parseInt(textID.getText()));
								st1.execute();
								Objects.errorComment.lblCommentPass
										.setText("UPDATED");
								Objects.errorComment.setVisible(true);
							} else {
								Objects.errorComment.lblCommentPass
										.setText("NOT UPDATED");
								Objects.errorComment.setVisible(true);
							}
							result.close();
							Objects.connection.close();
						} catch (SQLException e1) {
							// e1.printStackTrace();
						}
					} else {
						Objects.errorComment.lblCommentPass
								.setText("CHECK FIELDS");

						Objects.errorComment.setVisible(true);
					}
				}
			}
		});
		getContentPane().add(btnUpdate);

		JButton buttonDelete = new JButton("DELETE");
		buttonDelete.setBackground(Color.WHITE);
		buttonDelete.setBounds(12, 236, 104, 20);
		buttonDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rdbtnAdmin.isSelected()) {
					if (!textID.getText().isEmpty()) {
						Objects.connection = Model.Connection();
						PreparedStatement st1 = null;
						try {
							st1 = Objects.connection
									.prepareStatement("DELETE FROM admins WHERE id_admin=?;");
							st1.setInt(1, Integer.parseInt(textID.getText()));
							st1.execute();

							Objects.connection.close();
							Objects.errorComment.lblCommentPass
									.setText("DELETED");
							Objects.errorComment.setVisible(true);
						} catch (SQLException e1) {
							Objects.errorComment.lblCommentPass
									.setText("NOT DELETED");
							Objects.errorComment.setVisible(true);
							// e1.printStackTrace();
						}
					} else {
						Objects.errorComment.lblCommentPass
								.setText("CHECK FIELD ID");

						Objects.errorComment.setVisible(true);
					}
				}
				if (radioUser.isSelected()) {
					if (!textID.getText().isEmpty()) {
						Objects.connection = Model.Connection();
						PreparedStatement st1 = null;
						try {
							st1 = Objects.connection
									.prepareStatement("DELETE FROM users WHERE id_user=?;");
							st1.setInt(1, Integer.parseInt(textID.getText()));
							st1.execute();
							Objects.connection.close();
							Objects.errorComment.lblCommentPass
									.setText("DELETED");
							Objects.errorComment.setVisible(true);
						} catch (SQLException e1) {
							Objects.errorComment.lblCommentPass
									.setText("NOT DELETED");
							Objects.errorComment.setVisible(true);
							// e1.printStackTrace();
						}
					} else {
						Objects.errorComment.lblCommentPass
								.setText("CHECK FIELD ID");

						Objects.errorComment.setVisible(true);
					}
				}
			}
		});
		getContentPane().add(buttonDelete);

		JButton btnFind = new JButton("FIND");
		btnFind.setBackground(Color.WHITE);
		btnFind.setBounds(121, 236, 104, 20);
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.findUser.setVisible(true);
			}
		});
		getContentPane().add(btnFind);

		rdbtnAdmin = new JRadioButton("Admin");
		rdbtnAdmin.setForeground(Color.WHITE);
		rdbtnAdmin.setBounds(12, 6, 87, 23);
		getContentPane().add(rdbtnAdmin);
		transparentButton(rdbtnAdmin);

		radioUser = new JRadioButton("User");
		radioUser.setForeground(Color.WHITE);
		radioUser.setBounds(132, 6, 87, 23);
		getContentPane().add(radioUser);
		transparentButton(radioUser);
		buttonGroup.add(rdbtnAdmin);
		buttonGroup.add(radioUser);
		radioUser.setSelected(true);

		JButton btnBack = new JButton("BACK");
		btnBack.setBackground(Color.WHITE);
		btnBack.setBounds(12, 257, 213, 20);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.userManager.setVisible(false);
			}
		});
		getContentPane().add(btnBack);

		lblPosition = new JLabel("Position :");
		lblPosition.setForeground(Color.WHITE);
		lblPosition.setBounds(12, 168, 97, 15);
		getContentPane().add(lblPosition);
		lblPosition.setVisible(true);

		spinner = new JSpinner();
		spinner.setModel(new SpinnerListModel(Model.getPositions()));
		spinner.setBounds(111, 168, 114, 19);
		getContentPane().add(spinner);
		spinner.setVisible(true);

		JLabel lblStatus = new JLabel("Status :");
		lblStatus.setForeground(Color.WHITE);
		lblStatus.setBounds(12, 195, 97, 15);
		getContentPane().add(lblStatus);

		textStatus = new JTextField();
		textStatus.setColumns(10);
		textStatus.setBounds(111, 193, 114, 19);
		getContentPane().add(textStatus);

		Model.ImagePanel panel = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
		getContentPane().add(panel);
	}
}