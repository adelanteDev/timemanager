package admin;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.ListSelectionModel;

import model.Model;
import model.Objects;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UsersList extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable tTable;

	public UsersList() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 388, 290);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		JButton btnShow = new JButton("SHOW");
		btnShow.setBackground(Color.WHITE);
		btnShow.setBounds(242, 9, 124, 20);
		btnShow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshTable();
			}
		});
		contentPane.add(btnShow);

		JScrollPane spTable;
		tTable = new JTable();
		JPanel pTable = new JPanel();
		pTable.setBounds(12, 39, 354, 173);
		pTable.setLayout(new BorderLayout());
		tTable.setModel(new DefaultTableModel(new Object[9][10], new String[] {
				"ID", "NAME", "SURNAME", "NICK", "STATUS", "POSITION" }));
		spTable = new JScrollPane();
		spTable.setViewportView(tTable);
		pTable.add(spTable, BorderLayout.CENTER);
		tTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		getContentPane().add(pTable);

		JButton btnE = new JButton("EXPORT TO FILE");
		btnE.setBackground(Color.WHITE);
		btnE.setBounds(12, 224, 171, 25);
		btnE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					createFile();
				} catch (FileNotFoundException | UnsupportedEncodingException e1) {
					e1.printStackTrace();
				}
			}
		});
		contentPane.add(btnE);

		JButton btnBack = new JButton("BACK");
		btnBack.setBackground(Color.WHITE);
		btnBack.setBounds(195, 224, 171, 25);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.usersList.setVisible(false);
			}
		});
		contentPane.add(btnBack);

		Model.ImagePanel panel = new Model.ImagePanel(
				new ImageIcon("back1.jpg").getImage());
		getContentPane().add(panel);
	}

	public void refreshTable() {
		DefaultTableModel tableModel = (DefaultTableModel) tTable.getModel();
		tableModel.setNumRows(0);
		Objects.connection = Model.Connection();
		PreparedStatement st1 = null;
		PreparedStatement st2 = null;
		ResultSet result1 = null;
		try {
			st1 = Objects.connection
					.prepareStatement("SELECT id_user, name, surname, nick, status, id_position FROM users;");
			ResultSet result = st1.executeQuery();
			int i = 0;
			while (result.next()) {
				tableModel.addRow(new Object[] { 1 });
				tTable.setValueAt(result.getInt(1), i, 0);
				tTable.setValueAt(result.getString(2), i, 1);
				tTable.setValueAt(result.getString(3), i, 2);
				tTable.setValueAt(result.getString(4), i, 3);
				tTable.setValueAt(result.getString(5), i, 4);
				st2 = Objects.connection
						.prepareStatement("SELECT name FROM positions WHERE id_position="
								+ result.getInt(6) + ";");
				result1 = st2.executeQuery();
				while (result1.next()) {
					tTable.setValueAt(result1.getString(1), i, 5);
				}
				i++;
			}
			st1.close();
			st2.close();
			result.close();
			result1.close();
			Objects.connection.close();
		} catch (SQLException e1) {
			// e1.printStackTrace();
		}
	}

	public void createFile() throws FileNotFoundException,
			UnsupportedEncodingException {

		Objects.connection = Model.Connection();
		PreparedStatement st1 = null;
		PreparedStatement st2 = null;
		try {
			st1 = Objects.connection
					.prepareStatement("SELECT id_user, name, surname, nick, status, id_position FROM users;");
			ResultSet result = st1.executeQuery();
			ResultSet result1 = null;
			PrintWriter writer = new PrintWriter("Users List.txt", "UTF-8");
			writer.println("Actual users list:\n");
			writer.println("Id.\tName\tSurname\t\tNick\tStatus\tPosition\n");
			while (result.next()) {
				st2 = Objects.connection
						.prepareStatement("SELECT name FROM positions WHERE id_position="
								+ result.getInt(6) + ";");
				result1 = st2.executeQuery();
				while (result1.next()) {
					writer.println(result.getInt(1) + ".\t"
							+ result.getString(2) + "\t" + result.getString(3)
							+ "\t" + result.getString(4) + "\t"
							+ result.getString(5) + "\t" + result1.getString(1));
				}
			}
			writer.close();
			result.close();
			result1.close();
			st1.close();
			st2.close();
			Objects.connection.close();
		} catch (SQLException e1) {
			// e1.printStackTrace();
		}
	}
}