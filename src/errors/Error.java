package errors;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JButton;

import model.Model;
import model.Objects;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;

public class Error extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	public Error() 
	{
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 266, 108);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo( null );
		
		JLabel lblConnectioError = new JLabel("Connection Error");
		lblConnectioError.setForeground(Color.WHITE);
		lblConnectioError.setBounds(66, 12, 133, 15);
		contentPane.add(lblConnectioError);
		
		JButton refreshButton = new JButton("REFRESH");
		refreshButton.setBackground(Color.WHITE);
		refreshButton.setBounds(12, 39, 119, 25);
		refreshButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.connection = Model.Connection();
				if(Objects.connection!=null){
					Objects.window.frame.setVisible(true);
					Objects.error.setVisible(false);
				}
			}
		});
		contentPane.add(refreshButton);
		
		JButton exitButton = new JButton("EXIT");
		exitButton.setBackground(Color.WHITE);
		exitButton.setBounds(134, 39, 107, 25);
		exitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Objects.connection.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.exit(0);
			}
		});
		contentPane.add(exitButton);
		
		Model.ImagePanel panel = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
		getContentPane().add(panel);
	}
}
