package errors;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;

import model.Model;
import model.Objects;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class ErrorComment extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	public JLabel lblCommentPass;

	/**
	 * Create the frame.
	 */
	public ErrorComment() 
	{
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 285, 133);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo( null );
		
		lblCommentPass = new JLabel("Comment should not be empty.");
		lblCommentPass.setForeground(Color.WHITE);
		lblCommentPass.setBounds(30, 12, 227, 25);
		contentPane.add(lblCommentPass);
		
		JButton btnOk = new JButton("OK");
		btnOk.setBackground(Color.WHITE);
		btnOk.setBounds(82, 55, 117, 25);
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Objects.errorComment.setVisible(false);
			}
		});
		contentPane.add(btnOk);
		
		Model.ImagePanel panel = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
		contentPane.add(panel);
	}
	
}