package errors;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;

import model.Model;
import model.Objects;


public class ErrorDate extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public ErrorDate() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 521, 187);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo( null );
		
		JLabel lblDateConflictCheck = new JLabel("Date conflict. Check your date and pick right date for your leaves.");
		lblDateConflictCheck.setBounds(12, -16, 570, 169);
		contentPane.add(lblDateConflictCheck);
		
		JButton btnNewButton = new JButton("OK!");
		btnNewButton.setBounds(336, 116, 145, 25);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Objects.errorDate.setVisible(false);
			}
		});
		contentPane.add(btnNewButton);
		
		Model.ImagePanel panel = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
		contentPane.add(panel);
	}
}