package errors;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;

import model.Model;
import model.Objects;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ErrorLogin extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	public ErrorLogin() 
	{
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 258, 257);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo( null );
		
		JLabel lblNewLabel = new JLabel("Wrong password or login.");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(33, 12, 192, 87);
		contentPane.add(lblNewLabel);
		
		JButton btnRetry = new JButton("OK");
		btnRetry.setBounds(78, 111, 117, 25);
		contentPane.add(btnRetry);
		btnRetry.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Objects.errorLogin.setVisible(false);
			}
		});
		
		Model.ImagePanel panel = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
		contentPane.add(panel);
	}
}