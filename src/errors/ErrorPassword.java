package errors;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JButton;

import model.Model;
import model.Objects;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ErrorPassword extends JFrame 
{

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	public ErrorPassword() 
	{
		setTitle("Password");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 266, 108);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo( null );
		
		JLabel lblConnectioError = new JLabel("Uncorrect username or pass");
		lblConnectioError.setForeground(Color.WHITE);
		lblConnectioError.setBounds(12, 12, 229, 15);
		contentPane.add(lblConnectioError);
		
		JButton exitButton = new JButton("OK");
		exitButton.setBackground(Color.WHITE);
		exitButton.setBounds(12, 39, 229, 25);
		exitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.password.setVisible(false);
			}
		});
		contentPane.add(exitButton);
		
		Model.ImagePanel panel = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
		getContentPane().add(panel);
	}
}