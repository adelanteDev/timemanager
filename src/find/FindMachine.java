package find;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
 






import model.Model;
import model.Objects;
import admin.MachineManager;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
 
 
public class FindMachine extends JFrame {
 
        /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private JPanel contentPane;
        private JTextField textField;
 
        /**
         * Create the frame.
         */
        public FindMachine() {
        		setLocationRelativeTo( null );
                setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                setBounds(100, 100, 226, 134);
                contentPane = new JPanel();
                contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
                setContentPane(contentPane);
                contentPane.setLayout(null);
               
                JLabel lblNickname = new JLabel("Name :");
                lblNickname.setForeground(Color.WHITE);
                lblNickname.setBounds(24, 12, 85, 15);
                contentPane.add(lblNickname);
               
                textField = new JTextField();
                textField.setBounds(115, 10, 85, 19);
                contentPane.add(textField);
                textField.setColumns(10);
               
                JButton btnNewButton = new JButton("FIND");
                btnNewButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                    	if(!textField.getText().isEmpty()){
        					Objects.connection = Model.Connection();
        					PreparedStatement st1 = null;
        					try {
        						st1 = Objects.connection.prepareStatement("SELECT id_machine, name, status, id_position FROM machines WHERE name=?;");
        						st1.setString(1, textField.getText());
        						ResultSet result = st1.executeQuery();
        						int position_id=0;
        						while(result.next()){
        							MachineManager.textID.setText(Integer.toString(result.getInt(1)));
        							MachineManager.textName.setText(result.getString(2));
        							MachineManager.textNick.setText(result.getString(3));
        							position_id = result.getInt(4);
        						}
        						st1 = Objects.connection.prepareStatement("SELECT name FROM positions WHERE id_position="+position_id+";");
        						result = st1.executeQuery();
        						while(result.next()){
        							MachineManager.spinner.setValue(result.getString(1));
        						}
        						result.close();
        						Objects.connection.close();
        						Objects.findMachine.setVisible(false);
        					} catch (SQLException e1) {
        						//e1.printStackTrace();
        					}
        				}
        				else {
        					Objects.errorComment.lblCommentPass.setText("CHECK FIELD");
        					Objects.errorComment.setVisible(true);
        				}
                    }
                });
                btnNewButton.setBackground(Color.WHITE);
                btnNewButton.setBounds(24, 36, 176, 25);
                contentPane.add(btnNewButton);
               
                JButton btnBack = new JButton("BACK");
                btnBack.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                    	Objects.findMachine.setVisible(false);
                    }
                });
                btnBack.setBackground(Color.WHITE);
                btnBack.setBounds(24, 68, 176, 25);
                contentPane.add(btnBack);
               
                Model.ImagePanel panel1 = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
                getContentPane().add(panel1);
        }
}