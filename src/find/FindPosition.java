package find;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import model.Model;
import model.Objects;
import admin.PositionManager;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FindPosition extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;

	public FindPosition() 
	{
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 226, 134);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		JLabel lblNickname = new JLabel("Name :");
		lblNickname.setForeground(Color.WHITE);
		lblNickname.setBounds(24, 12, 85, 15);
		contentPane.add(lblNickname);

		textField = new JTextField();
		textField.setBounds(115, 10, 85, 19);
		contentPane.add(textField);
		textField.setColumns(10);

		JButton btnNewButton = new JButton("FIND");
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setBounds(24, 36, 176, 25);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textField.getText().isEmpty()) {
					Objects.connection = Model.Connection();
					PreparedStatement st1 = null;
					try {
						st1 = Objects.connection
								.prepareStatement("SELECT id_position, name FROM positions WHERE name=?;");
						st1.setString(1, textField.getText());
						ResultSet result = st1.executeQuery();
						while (result.next()) {
							PositionManager.textID.setText(Integer
									.toString(result.getInt(1)));
							PositionManager.textName.setText(result
									.getString(2));
						}
						result.close();
						Objects.connection.close();
						Objects.findPosition.setVisible(false);
					} catch (SQLException e1) {
						// e1.printStackTrace();
					}
				} else {
					Objects.errorComment.lblCommentPass.setText("CHECK FIELD");
					Objects.errorComment.setVisible(true);
				}
			}
		});
		contentPane.add(btnNewButton);

		JButton btnBack = new JButton("BACK");
		btnBack.setBackground(Color.WHITE);
		btnBack.setBounds(24, 68, 176, 25);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.findPosition.setVisible(false);
			}
		});
		contentPane.add(btnBack);

		Model.ImagePanel panel1 = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
		getContentPane().add(panel1);
	}
}