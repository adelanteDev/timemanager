package find;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import model.Model;
import model.Objects;
import admin.UserManager;

public class FindUser extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	JRadioButton rdbtnAdmin;
	JRadioButton rdbtnUser;


	public void transparentButton(JRadioButton button) {
		button.setOpaque(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
	}

	public FindUser() 
	{
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 226, 172);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		JLabel lblNickname = new JLabel("Nickname :");
		lblNickname.setForeground(Color.WHITE);
		lblNickname.setBounds(24, 44, 85, 15);
		contentPane.add(lblNickname);

		textField = new JTextField();
		textField.setBounds(115, 42, 85, 19);
		contentPane.add(textField);
		textField.setColumns(10);

		JButton btnNewButton = new JButton("FIND");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rdbtnUser.isSelected()) {
					UserManager.radioUser.setSelected(true);
					UserManager.spinner.setVisible(true);
					UserManager.lblPosition.setVisible(true);
					if (!textField.getText().isEmpty()) {
						Objects.connection = Model.Connection();
						PreparedStatement st1 = null;
						Objects.findUser.setVisible(false);
						try {
							st1 = Objects.connection
									.prepareStatement("SELECT id_user, name, surname, nick, passwd, status, id_position FROM users WHERE nick=?;");
							st1.setString(1, textField.getText());
							ResultSet result = st1.executeQuery();
							int position_id = 0;
							while (result.next()) {
								UserManager.textID.setText(Integer
										.toString(result.getInt(1)));
								UserManager.textName.setText(result
										.getString(2));
								UserManager.textSurname.setText(result
										.getString(3));
								UserManager.textNick.setText(result
										.getString(4));
								UserManager.textPassword.setText(result
										.getString(5));
								UserManager.textStatus.setText(result
										.getString(6));
								position_id = result.getInt(7);
							}
							st1 = Objects.connection
									.prepareStatement("SELECT name FROM positions WHERE id_position="
											+ position_id + ";");
							result = st1.executeQuery();
							while (result.next()) {
								UserManager.spinner.setValue(result
										.getString(1));
							}

							result.close();
							Objects.connection.close();
						} catch (SQLException e1) {
							// e1.printStackTrace();
						}
					} else {
						Objects.errorComment.lblCommentPass
								.setText("CHECK FIELD");
						Objects.errorComment.setVisible(true);
					}
				}
				if (rdbtnAdmin.isSelected()) {
					UserManager.rdbtnAdmin.setSelected(true);
					UserManager.spinner.setVisible(false);
					UserManager.lblPosition.setVisible(false);
					if (!textField.getText().isEmpty()) {
						Objects.connection = Model.Connection();
						PreparedStatement st1 = null;
						Objects.findUser.setVisible(false);
						try {
							st1 = Objects.connection
									.prepareStatement("SELECT id_admin, name, surname, nick, passwd, status FROM admins WHERE nick=?;");
							st1.setString(1, textField.getText());
							ResultSet result = st1.executeQuery();
							while (result.next()) {
								UserManager.textID.setText(Integer
										.toString(result.getInt(1)));
								UserManager.textName.setText(result
										.getString(2));
								UserManager.textSurname.setText(result
										.getString(3));
								UserManager.textNick.setText(result
										.getString(4));
								UserManager.textPassword.setText(result
										.getString(5));
								UserManager.textStatus.setText(result
										.getString(6));
							}
							result.close();
							Objects.connection.close();
						} catch (SQLException e1) {
							// e1.printStackTrace();
						}
					} else
						Objects.errorComment.setVisible(true);
				}
			}
		});
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setBounds(24, 71, 176, 25);
		contentPane.add(btnNewButton);

		JButton btnBack = new JButton("BACK");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.findUser.setVisible(false);
			}
		});
		btnBack.setBackground(Color.WHITE);
		btnBack.setBounds(24, 108, 176, 25);
		contentPane.add(btnBack);

		rdbtnAdmin = new JRadioButton("Admin");
		rdbtnAdmin.setBounds(22, 13, 76, 23);
		rdbtnAdmin.setForeground(Color.WHITE);
		contentPane.add(rdbtnAdmin);

		rdbtnUser = new JRadioButton("User");
		rdbtnUser.setBounds(124, 13, 76, 23);
		contentPane.add(rdbtnUser);
		rdbtnUser.setForeground(Color.WHITE);
		transparentButton(rdbtnUser);
		transparentButton(rdbtnAdmin);
		buttonGroup.add(rdbtnAdmin);
		buttonGroup.add(rdbtnUser);
		Model.ImagePanel panel1 = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
		getContentPane().add(panel1);
	}
}