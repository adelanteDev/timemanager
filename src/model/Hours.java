package model;

import java.util.Date;


public class Hours {

	int id_user = 0;
	Date date;
	String name = "";
	String surname = "";
	String nick = "";
	long seconds = 0;
	
	public Hours(){
		id_user = 0;
		date = new Date();
		name = "";
		surname = "";
		nick = "";
		seconds = 0;
	}
	
	public int getId_user() {
		return id_user;
	}
	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public long getSeconds() {
		return seconds;
	}
	public void setSeconds(long seconds) {
		this.seconds = seconds;
	}

}
