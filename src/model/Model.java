package model;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.sql.*;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Model 
{

	public static Connection Connection() {

		Objects.connection = null;

		try {

			Class.forName("org.postgresql.Driver");

			Objects.connection = DriverManager
					.getConnection("jdbc:postgresql://ec2-107-20-244-39.compute-1.amazonaws.com:5432/d1ui2emjllquke?user=tkmzzcqmjxkbhu&password=ZXjsjl5mAY5B7d0VZeF_Db-2mp&ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory");

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return Objects.connection;
	}

	public static String[] getMachines() {
		String[] machines = new String[1000];
		Objects.connection = Model.Connection();
		PreparedStatement st1 = null;
		int i = 0;
		try {
			st1 = Objects.connection
					.prepareStatement("SELECT name FROM machines WHERE status='OK' AND id_position="
							+ Objects.id_position + ";");
			ResultSet result = st1.executeQuery();
			while (result.next()) {
				machines[i] = result.getString(1);
				i++;
			}
			result.close();
			Objects.connection.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return machines;
	}

	public static int getMachineId(String name) {
		int id = -1;
		Objects.connection = Model.Connection();
		PreparedStatement st1 = null;
		try {
			st1 = Objects.connection
					.prepareStatement("SELECT id_machine FROM machines WHERE name='"
							+ name + "';");
			ResultSet result = st1.executeQuery();
			while (result.next()) {
				id = result.getInt(1);
			}
			result.close();
			Objects.connection.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return id;
	}

	public static String[] getPositions() {
		String[] positions = new String[1000];
		Objects.connection = Model.Connection();
		PreparedStatement st1 = null;
		int i = 0;
		try {
			st1 = Objects.connection
					.prepareStatement("SELECT name FROM positions;");
			ResultSet result = st1.executeQuery();
			while (result.next()) {
				positions[i] = result.getString(1);
				i++;
			}
			result.close();
			Objects.connection.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return positions;
	}

	public static int getPositionId(String name) {
		int id = -1;
		Objects.connection = Model.Connection();
		PreparedStatement st1 = null;
		try {
			st1 = Objects.connection
					.prepareStatement("SELECT id_position FROM positions WHERE name='"
							+ name + "';");
			ResultSet result = st1.executeQuery();
			while (result.next()) {
				id = result.getInt(1);
			}
			result.close();
			Objects.connection.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return id;
	}

	public static boolean checkLeaveDates(String from, String to) {
		boolean status = false;
		Objects.connection = Model.Connection();
		PreparedStatement st1 = null;
		try {
			st1 = Objects.connection.prepareStatement("SELECT CURRENT_DATE;");
			ResultSet result = st1.executeQuery();
			String date = "";
			while (result.next()) {
				date = result.getDate(1).toString();
			}
			result.close();
			Objects.connection.close();
			if (from.substring(0, 4).equals(date.substring(0, 4))) {
				if (from.substring(5, 7).equals(date.substring(5, 7))) {
					if (Integer.parseInt(from.substring(8, 10).toString()) >= Integer
							.parseInt(date.substring(8, 10).toString())) {
						if (from.substring(0, 4).equals(to.substring(0, 4))) {
							if (from.substring(5, 7).equals(to.substring(5, 7))) {
								if ((Integer.parseInt(from.substring(8, 10))) < (Integer
										.parseInt(to.substring(8, 10)))) {
									status = true;
								} else
									status = false;
							} else if (Integer.parseInt(from.substring(5, 7)) < Integer
									.parseInt(to.substring(5, 7))) {
								status = true;
							} else
								status = false;
						} else if (Integer.parseInt(from.substring(0, 4)) < Integer
								.parseInt(to.substring(0, 4))) {
							status = true;
						} else
							status = false;
					}
				}
				if (Integer.parseInt(from.substring(5, 7).toString()) > Integer
						.parseInt(date.substring(5, 7).toString())) {
					if (from.substring(0, 4).equals(to.substring(0, 4))) {
						if (from.substring(5, 7).equals(to.substring(5, 7))) {
							if (Integer.parseInt(from.substring(8, 10)) < Integer
									.parseInt(to.substring(8, 10))) {
								status = true;
							} else
								status = false;
						} else if (Integer.parseInt(from.substring(5, 7)) < Integer
								.parseInt(to.substring(5, 7))) {
							status = true;
						} else
							status = false;
					} else if (Integer.parseInt(from.substring(0, 4)) < Integer
							.parseInt(to.substring(0, 4))) {
						status = true;
					} else
						status = false;
				}
			} else if (Integer.parseInt(from.substring(0, 4).toString()) > Integer
					.parseInt(date.substring(0, 4).toString())) {
				if (from.substring(0, 4).equals(to.substring(0, 4))) {
					if (from.substring(5, 7).equals(to.substring(5, 7))) {
						if (Integer.parseInt(from.substring(8, 10)) < Integer
								.parseInt(to.substring(8, 10))) {
							status = true;
						} else
							status = false;
					} else if (Integer.parseInt(from.substring(5, 7)) < Integer
							.parseInt(to.substring(5, 7))) {
						status = true;
					} else
						status = false;
				} else if (Integer.parseInt(from.substring(0, 4)) < Integer
						.parseInt(to.substring(0, 4))) {
					status = true;
				} else
					status = false;
			} else
				status = false;
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return status;
	}

	public static class ImagePanel extends JPanel {
		private static final long serialVersionUID = 1L;
		private Image img;

		public ImagePanel(String img) {
			this(new ImageIcon(img).getImage());
		}

		public ImagePanel(Image img) {
			this.img = img;
			Dimension size = new Dimension(img.getWidth(null),
					img.getHeight(null));
			setPreferredSize(size);
			setMinimumSize(size);
			setMaximumSize(size);
			setSize(size);
			setLayout(null);
		}

		public void paintComponent(Graphics g) {
			g.drawImage(img, 0, 0, null);
		}
	}

}
