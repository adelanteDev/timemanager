package model;
import java.sql.Connection;

import errors.Error;
import errors.ErrorComment;
import errors.ErrorDate;
import errors.ErrorLogin;
import errors.ErrorPassword;
import find.FindMachine;
import find.FindPosition;
import find.FindUser;
import user.LeavesUser;
import user.Login;
import user.User;
import admin.Admin;
import admin.AdminsList;
import admin.CommentsList;
import admin.LeavesList;
import admin.MachineManager;
import admin.MachinesList;
import admin.PositionManager;
import admin.PositionsList;
import admin.UserHours;
import admin.UserManager;
import admin.UsersList;


public class Objects {
	
	public static Connection connection;
	
	public static int id;
	public static String name;
	public static String surname;
	public static String nick;
	public static String passwd;
	public static int id_position;
	
	public static String[] machines = new String[1000];
	
	public static User user = new User();
	public static Admin admin = new Admin();
	public static Login window = new Login();
	public static Error error = new Error();
	public static ErrorPassword password = new ErrorPassword();
	public static LeavesUser leavesUser = new LeavesUser();
	public static ErrorDate errorDate = new ErrorDate();
	public static ErrorLogin errorLogin = new ErrorLogin();
	public static ErrorComment errorComment = new ErrorComment();
	public static UserManager userManager = new UserManager();
	public static MachineManager machineManager = new MachineManager();
	public static FindUser findUser = new FindUser();
	public static FindMachine findMachine = new FindMachine();
	public static PositionManager positionManager = new PositionManager();
	public static FindPosition findPosition = new FindPosition();
	public static UserHours userHours = new UserHours();
	public static UsersList usersList = new UsersList();
	public static MachinesList machinesList = new MachinesList();
	public static PositionsList positionsList = new PositionsList();
	public static LeavesList leavesList = new LeavesList();
	public static CommentsList commentsList = new CommentsList();
	public static AdminsList adminsList = new AdminsList();
	
	public static void ResetObjects()
	{
		id = 0;
		name = null;
		surname = null;
		nick = null;
		passwd = null;
		id_position = 0;
	}
}
