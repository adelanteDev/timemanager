package time_manager;
import java.awt.EventQueue;

import model.Model;
import model.Objects;
import user.User;


public class Program {
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Objects.window.frame.setVisible(true);
					Objects.connection = Model.Connection();
					if(Objects.connection==null){
						Objects.window.frame.setVisible(false);
						Objects.error.setVisible(true);
					}
					User.startTime();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
