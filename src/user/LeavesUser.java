package user;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;

import model.Model;
import model.Objects;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;

public class LeavesUser extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	public JDatePickerImpl calendarioF;
	public JDatePanelImpl panelCalendarF;
	public JDatePickerImpl calendarioT;
	public JDatePanelImpl panelCalendarT;

	public LeavesUser() 
	{
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 453, 184);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		
		textField = new JTextField();
		textField.setBounds(217, 39, 208, 88);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblSs = new JLabel("From :");
		lblSs.setBounds(24, 12, 70, 15);
		lblSs.setForeground(Color.WHITE);
		contentPane.add(lblSs);
		
		JLabel lblTo = new JLabel("To :");
		lblTo.setBounds(24, 60, 70, 15);
		lblTo.setForeground(Color.WHITE);
		contentPane.add(lblTo);
		
		JLabel lblReason = new JLabel("Reason :");
		lblReason.setBounds(217, 12, 70, 15);
		lblReason.setForeground(Color.WHITE);
		contentPane.add(lblReason);
		
		panelCalendarF=new JDatePanelImpl(null);
		calendarioF=new JDatePickerImpl(panelCalendarF);
		calendarioF.setBounds(85, 12, 120, 30);
		contentPane.add(calendarioF);
		
		panelCalendarT=new JDatePanelImpl(null);
		calendarioT=new JDatePickerImpl(panelCalendarT);
		SpringLayout springLayout = (SpringLayout) calendarioT.getLayout();
		springLayout.putConstraint(SpringLayout.SOUTH, calendarioT.getJFormattedTextField(), 0, SpringLayout.SOUTH, calendarioT);
		calendarioT.setBounds(85, 60, 120, 30);		
		contentPane.add(calendarioT);
		
		JButton btnSend = new JButton("OK");
		btnSend.setBounds(24, 102, 181, 25);
		btnSend.setBackground(Color.WHITE);
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				java.util.GregorianCalendar selectedDate1 = (java.util.GregorianCalendar) panelCalendarF
						.getModel().getValue();
				java.util.GregorianCalendar selectedDate2 = (java.util.GregorianCalendar) panelCalendarT
						.getModel().getValue();
				if (selectedDate1 != null && selectedDate2 != null) {
					Date selectedDate = selectedDate1.getTime();
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					String from = df.format(selectedDate);

					Date selectedDate3 = selectedDate2.getTime();
					DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
					String to = df1.format(selectedDate3);

					if (Model.checkLeaveDates(from, to)) {
						if (!textField.getText().isEmpty()) {
							Objects.connection = Model.Connection();
							PreparedStatement st1 = null;
							try {
								st1 = Objects.connection
										.prepareStatement("INSERT INTO leaves (id_user, leave_from, leave_to, reason) VALUES ("
												+ Objects.id
												+ ",'"
												+ from
												+ "','" + to + "',?);");
								st1.setString(1, textField.getText());
								st1.execute();
								Objects.connection.close();
								textField.setText("");
								Objects.leavesUser.setVisible(false);
							} catch (SQLException e1) {
								// e1.printStackTrace();
							}
						} else {
							Objects.errorComment.lblCommentPass
									.setText("REASON CAN NOT BE EMPTY");

							Objects.errorComment.setVisible(true);
						}
					} else {
						Objects.errorComment.lblCommentPass
								.setText("CHECK DATES AND REPAIR");

						Objects.errorComment.setVisible(true);
					}
				} else {
					Objects.errorComment.lblCommentPass
							.setText("DATES CAN NOT BE EMPTY.");
					Objects.errorComment.setVisible(true);
				}
			}
		});
		contentPane.add(btnSend);

		Model.ImagePanel panel = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
		contentPane.add(panel);
	}
}