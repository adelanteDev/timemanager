package user;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.SpinnerListModel;

import model.Model;
import model.Objects;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

public class Login {
	public JFrame frame;
	private JPasswordField pwdPassword;
	private JTextField textField;
	private JRadioButton rdbtnAdmin;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbtnUser;

	public Login() {
		initialize();
	}

	public void transparentButton(JRadioButton button) {
		button.setOpaque(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
	}

	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 431, 301);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);

		JLabel lblName = new JLabel("Name :");
		lblName.setForeground(Color.WHITE);
		lblName.setBounds(124, 101, 70, 15);
		frame.getContentPane().add(lblName);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setBounds(124, 128, 88, 15);
		frame.getContentPane().add(lblPassword);

		pwdPassword = new JPasswordField();
		pwdPassword.setBounds(208, 126, 94, 19);
		pwdPassword.setText("pass");
		frame.getContentPane().add(pwdPassword);

		textField = new JTextField();
		textField.setBounds(208, 99, 94, 19);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		rdbtnAdmin = new JRadioButton("Admin");
		rdbtnAdmin.setForeground(Color.WHITE);
		rdbtnAdmin.setBounds(124, 60, 70, 23);
		transparentButton(rdbtnAdmin);
		frame.getContentPane().add(rdbtnAdmin);

		rdbtnUser = new JRadioButton("User");
		rdbtnUser.setForeground(Color.WHITE);
		transparentButton(rdbtnUser);
		rdbtnUser.setBounds(232, 60, 70, 23);
		frame.getContentPane().add(rdbtnUser);
		buttonGroup.add(rdbtnAdmin);
		buttonGroup.add(rdbtnUser);

		JButton exittButton = new JButton("EXIT");
		exittButton.setBounds(124, 192, 178, 25);
		exittButton.setBackground(Color.WHITE);
		exittButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		frame.getContentPane().add(exittButton);

		JButton btnOk = new JButton("OK");
		btnOk.setBackground(Color.WHITE);
		btnOk.setBounds(124, 155, 178, 25);
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rdbtnUser.isSelected()) {
					try {
						Objects.connection = Model.Connection();
						if (Objects.connection != null) {
							PreparedStatement st = null;
							st = Objects.connection
									.prepareStatement("SELECT id_user,name,surname,nick,passwd,id_position FROM users WHERE nick=?;");
							st.setString(1, textField.getText());
							Objects.ResetObjects();
							ResultSet result = st.executeQuery();
							if (result != null) {
								while (result.next()) {
									Objects.id = result.getInt(1);
									Objects.name = result.getString(2);
									Objects.surname = result.getString(3);
									Objects.nick = result.getString(4);
									Objects.passwd = result.getString(5);
									Objects.id_position = result.getInt(6);
								}
								if (Objects.passwd != null) {
									if ((Arrays.equals(
											Objects.passwd.toCharArray(),
											pwdPassword.getPassword()))
											&& (textField.getText()
													.equals(Objects.nick))) {
										User.lblWelcome.setText("Hello "
												+ Objects.name + " "
												+ Objects.surname + "!");
										Objects.machines = Model.getMachines();
										User.spinner
												.setModel(new SpinnerListModel(
														Objects.machines/*
																		 * Model.
																		 * getMachines
																		 * ()
																		 */));
										Objects.user.setVisible(true);
										Objects.window.frame.setVisible(false);
									} else {
										Objects.password.setVisible(true);
									}
								} else {
									Objects.errorComment.lblCommentPass
											.setText("LOGIN AND PASSWORD INCORRECT");
									Objects.errorComment.setVisible(true);
								}
							} else {
								Objects.password.setVisible(true);
							}
							st.close();
							Objects.connection.close();
						} else if (Objects.connection == null) {
							Objects.error.setVisible(true);
							Objects.window.frame.setVisible(false);
						}
					} catch (SQLException e1) {
						// e1.printStackTrace();
						Objects.password.setVisible(true);
					}
				} else if (rdbtnAdmin.isSelected()) {
					try {
						Objects.connection = Model.Connection();
						if (Objects.connection != null) {
							PreparedStatement st = null;
							st = Objects.connection
									.prepareStatement("SELECT id_admin,name,surname,nick,passwd FROM admins WHERE nick=?;");
							st.setString(1, textField.getText());
							Objects.ResetObjects();
							ResultSet result = st.executeQuery();
							if (result != null) {
								while (result.next()) {
									Objects.id = result.getInt(1);
									Objects.name = result.getString(2);
									Objects.surname = result.getString(3);
									Objects.nick = result.getString(4);
									Objects.passwd = result.getString(5);
								}
								if (Objects.passwd != null) {
									if (Arrays.equals(
											Objects.passwd.toCharArray(),
											pwdPassword.getPassword())
											&& (textField.getText()
													.equals(Objects.nick))) {
										Objects.admin.setVisible(true);
										Objects.window.frame.setVisible(false);
									} else {
										Objects.password.setVisible(true);
									}
								} else {
									Objects.errorComment.lblCommentPass
											.setText("LOGIN AND PASSWORD INCORRECT");
									Objects.errorComment.setVisible(true);
								}
							} else {
								Objects.password.setVisible(true);
							}
							st.close();
							Objects.connection.close();
						} else if (Objects.connection == null) {
							Objects.error.setVisible(true);
							Objects.window.frame.setVisible(false);
						}
					} catch (SQLException e1) {
						// e1.printStackTrace();
						Objects.password.setVisible(true);
					}
				}
			}
		});
		frame.getContentPane().add(btnOk);

		Model.ImagePanel panel = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
		frame.getContentPane().add(panel);
	}
}
