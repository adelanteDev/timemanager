package user;

import javax.swing.*;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.border.LineBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JLabel;

import java.awt.Color;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;

import model.Model;
import model.Objects;

import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;

public class User extends JFrame 
{
	private static final long serialVersionUID = 1L;

	JButton btnStart;
	JButton btnStop;
	JButton btnLogout;
	JButton btnComment;
	JButton btnLeave;
	JTextArea textArea;
	static JSpinner spinner;
	JLabel lblWorkTimeN ;
	java.util.Date date;
	String time;
	String time1;
	Time time_1;
	Time time_2;
	
	private static JLabel lblTime = new JLabel("");
	static JLabel lblWorkTime = new JLabel("");
	static JLabel lblWelcome = new JLabel("Hello "+Objects.name);
	static JPanel panelLabel;
	
	private javax.swing.Timer myTimer1;  
	public static final int TENTH_SEC = 100;
	private int clockTick;
	private double nSeconds;  	
	private int nMinutes = 0;
	private int nHours = 0;
	
	public static void startTime()
	{
		lTime ltime = new lTime();
		ltime.start();
	}
	
	public void stopWatchReset() 
	{
		clockTick = 0;
		nSeconds = 0;
		nMinutes = 0;
		nHours = 0;
		nSeconds = ((double) clockTick) / 10.0;
		new Double(nSeconds).toString();
	}

	public User() 
	{
		setTitle("User");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 392, 300);
		getContentPane().setLayout(null);
		setResizable(false);
		setLocationRelativeTo(null);
		
		btnLogout = new JButton("LOGOUT");
		btnLogout.setBackground(Color.WHITE);
		btnLogout.setBounds(248, 221, 117, 25);
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.user.setVisible(false);
				Objects.window.frame.setVisible(true);
				Objects.ResetObjects();
			}
		});
		getContentPane().add(btnLogout);
		
		btnStart = new JButton("START");
		btnStart.setBackground(Color.WHITE);
		btnStart.setBounds(12, 12, 133, 25);
		btnStart.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				btnStop.setVisible(true);
				btnStart.setVisible(false);
				btnLogout.setVisible(false);
				spinner.setEnabled(false);
				Objects.connection = Model.Connection();
				PreparedStatement st1 = null;
				try {
					stopWatchReset();
					st1 = Objects.connection.prepareStatement("SELECT CURRENT_DATE;");
					ResultSet result = st1.executeQuery();
					date = new java.util.Date();
					while(result.next()){
						date = result.getDate(1);
					}
					st1 = Objects.connection.prepareStatement("SELECT CURRENT_TIME(1);");
					result = st1.executeQuery();
					int hours = 0;
					int minutes = 0;
					int seconds = 0;
					while(result.next()){
						hours = result.getTime(1).getHours()+1; //GMT +1
						minutes = result.getTime(1).getMinutes();
						seconds = result.getTime(1).getSeconds();
					}
					time_1 = new Time(hours,minutes,seconds);
					time = "";
					if(hours<=9){
						time = time + "0"+Integer.toString(hours)+":";
					} else{
						time =  time + Integer.toString(hours)+":";
					}
					if(minutes<=9){
						time = time + "0"+Integer.toString(minutes)+":";
					} else{
						time = time + Integer.toString(minutes)+":";
					}
					if(seconds<=9){
						time = time + "0"+Integer.toString(seconds);
					} else{
						time = time + Integer.toString(seconds);
					}		
					st1 = Objects.connection.prepareStatement("INSERT INTO hours (id_user, date, start, id_machine) VALUES ("+Objects.id+",'"+date+"','"+time+"',"+Model.getMachineId(spinner.getValue().toString())+");");
					st1.execute();
					myTimer1.start();
					result.close();
					Objects.connection.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		getContentPane().add(btnStart);
		
		btnStop = new JButton("STOP");
		btnStop.setBackground(Color.WHITE);
		btnStop.setBounds(12, 12, 133, 25);
		btnStop.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				btnStop.setVisible(false);
				btnStart.setVisible(true);
				btnLogout.setVisible(true);
				spinner.setEnabled(true);
				Objects.connection = Model.Connection();
				PreparedStatement st1 = null;
				try {
					st1 = Objects.connection.prepareStatement("SELECT CURRENT_TIME(1);");
					ResultSet result = st1.executeQuery();
					int hours = 0;
					int minutes = 0;
					int seconds = 0;
					while(result.next()){
						hours = result.getTime(1).getHours()+1; //GMT +1
						minutes = result.getTime(1).getMinutes();
						seconds = result.getTime(1).getSeconds();
					}
					time_2 = new Time(hours,minutes,seconds);
					long worked_time = (time_2.getTime() - time_1.getTime())/1000;
					time1 = "";
					if(hours<=9){
						time1 = time1 + "0"+Integer.toString(hours)+":";
					} else{
						time1 =  time1 + Integer.toString(hours)+":";
					}
					if(minutes<=9){
						time1 = time1 + "0"+Integer.toString(minutes)+":";
					} else{
						time1 = time1 + Integer.toString(minutes)+":";
					}
					if(seconds<=9){
						time1 = time1 + "0"+Integer.toString(seconds);
					} else{
						time1 = time1 + Integer.toString(seconds);
					}		
					st1 = Objects.connection.prepareStatement("SELECT id_hour FROM hours WHERE id_user="+Objects.id+" AND date='"+date+"' AND start='"+time+"';");
					result = st1.executeQuery();
					int id_hour = 0;
					while(result.next()){
						id_hour = result.getInt(1);
					}
					myTimer1.stop();
					st1 = Objects.connection.prepareStatement("UPDATE hours SET stop='"+time1+"', seconds="+worked_time+" WHERE id_hour="+id_hour+";");
					st1.execute();
					result.close();
					Objects.connection.close();
					
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		getContentPane().add(btnStop);
		
		btnComment = new JButton("ADD COMMENT");
		btnComment.setBackground(Color.WHITE);
		btnComment.setBounds(174, 85, 201, 25);
		btnComment.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				if(!textArea.getText().isEmpty()){
					Objects.connection = Model.Connection();
					PreparedStatement st1 = null;
					try {
						st1 = Objects.connection.prepareStatement("SELECT CURRENT_DATE;");
						ResultSet result = st1.executeQuery();
						date = new java.util.Date();
						while(result.next()){
							date = result.getDate(1);
						}
						st1 = Objects.connection.prepareStatement("SELECT CURRENT_TIME(1);");
						result = st1.executeQuery();
						int hours = 0;
						int minutes = 0;
						int seconds = 0;
						while(result.next()){
							hours = result.getTime(1).getHours()+1; //GMT +1
							minutes = result.getTime(1).getMinutes();
							seconds = result.getTime(1).getSeconds();
						}
						time = "";
						if(hours<=9){
							time = time + "0"+Integer.toString(hours)+":";
						} else{
							time =  time + Integer.toString(hours)+":";
						}
						if(minutes<=9){
							time = time + "0"+Integer.toString(minutes)+":";
						} else{
							time = time + Integer.toString(minutes)+":";
						}
						if(seconds<=9){
							time = time + "0"+Integer.toString(seconds);
						} else{
							time = time + Integer.toString(seconds);
						}
						st1 = Objects.connection.prepareStatement("INSERT INTO comments (id_user, date, time, id_machine, comment) VALUES ("+Objects.id+",'"+date+"','"+time+"',"+Model.getMachineId(spinner.getValue().toString())+",?);");
						st1.setString(1, textArea.getText());
						st1.execute();
						result.close();
						Objects.connection.close();
						textArea.setText("");
						Objects.errorComment.lblCommentPass.setText("COMMENT HAS BEEN ADDED");
						Objects.errorComment.setVisible(true);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
				else {
					Objects.errorComment.lblCommentPass.setText("COMMENT SHOULD NOT BE EMPTY.");
					Objects.errorComment.setVisible(true);
				}
			}
		});
		getContentPane().add(btnComment);
		
		btnLeave = new JButton("LEAVE");
		btnLeave.setBackground(Color.WHITE);
		btnLeave.setBounds(174, 122, 201, 25);
		btnLeave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Objects.leavesUser.setVisible(true);
			}
		});
		getContentPane().add(btnLeave);
		
		textArea = new JTextArea();
		textArea.setText("");
		textArea.setBounds(174, 12, 201, 61);
		getContentPane().add(textArea);
		
		lblWelcome.setForeground(Color.WHITE);
		lblWelcome.setBounds(10, 49, 150, 15);
		getContentPane().add(lblWelcome);

		panelLabel = new JPanel();
		panelLabel.setBorder(new LineBorder(Color.GRAY, 1));
		panelLabel.setBackground(Color.WHITE);
		panelLabel.setBounds(12, 85, 133, 99);
		panelLabel.setLayout(null);
		getContentPane().add(panelLabel);
		
		lblWorkTimeN = new JLabel("Work time:");
		lblWorkTimeN.setBounds(12, 54, 109, 15);
		panelLabel.add(lblWorkTimeN);
		
		lblWorkTime.setForeground(Color.BLACK);
		lblWorkTime.setBounds(12, 71, 100, 15);
		panelLabel.add(lblWorkTime);
		
		spinner	= new JSpinner();
		spinner.setModel(new SpinnerListModel(Objects.machines));
		((DefaultEditor) spinner.getEditor()).getTextField().setEditable(false);
		spinner.setBounds(174, 159, 201, 25);
		getContentPane().add(spinner);
		
		//StopWatch
		clockTick = 0;
		nSeconds = ((double)clockTick)/10.0;
		new Double(nSeconds).toString();

		myTimer1 = new javax.swing.Timer(TENTH_SEC, new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
			clockTick++;
			
			nSeconds = ((double)clockTick)/10.0;
		
				if(nSeconds == 60)
				{
            		nMinutes++;
            		clockTick = 0;
            		nSeconds = 0;
            	}
            	if(nMinutes == 60)
            	{
            		nMinutes = 0 ;
            		nHours++;
            	}
            	new Double(nSeconds).toString();
			lblWorkTime.setText(String.format("0%s:0%s:%s", String.valueOf(nHours), String.valueOf(nMinutes),String.valueOf(nSeconds++)));
		    }
		});

		Model.ImagePanel panel1 = new Model.ImagePanel(new ImageIcon("back1.jpg").getImage());
		getContentPane().add(panel1);
	}
	
	
	static class lTime 
	{
		
		private JLabel lblTimeN = new JLabel("Time:");
		private SimpleDateFormat sdf1  = new SimpleDateFormat("hh:mm");
		int currentSecond1;
		Calendar calendar1;
		
		public lTime()
		{
			lblTimeN.setBounds(12, 12, 38, 15);
			lblTimeN.setForeground(Color.BLACK);
			panelLabel.add(lblTimeN);
			
			lblTime.setForeground(Color.BLACK);
			lblTime.setBounds(12, 27, 70, 15);
			panelLabel.add(lblTime);
		}
		
		private void reset()
		{
	        calendar1 = Calendar.getInstance();
	        currentSecond1 = calendar1.get(Calendar.SECOND);
		}
	    
		public void start()
		{
			reset();
		    Timer timer = new Timer();
		    timer.scheduleAtFixedRate( new TimerTask()
		    {
		    public void run()
		    {
		    	if( currentSecond1 == 60 ) 
		    	{
		    		reset();
		        }
		        lblTime.setText(String.format("%s:%02d", sdf1.format(calendar1.getTime()), currentSecond1 ));
		        currentSecond1++;
		        }
		    }, 0, 1000 );
		}
	}
}